# Aufgabe 4 (Qualifizierung im Team)

Realisieren Sie eine Javalin-Anwendung im Team nach Ihren gemeinsamen Vorstellungen:

**Lernziele**:

Ziel der Projektarbeit für die Qualifizierungsphase ist es, sich anhand einer konkreten Anwendungsidee weiter einzuarbeiten in die Möglichkeiten,

* die Javalin zur Umsetzung von Web-Anwendungen bietet,
* die externe Angebote oder Frameworks zur Verfügung stellen,
* die ein modern Java/Kotlin-Programmierstil mit sich bringt.

Sie lernen darüber hinaus, eine Anwendung in vier gedankliche Anteile zu zerlegen: die Idee, Oberflächen-Entwurf, API-Entwurf, Interface-Entwurf.

Das Projekt ist als Teamprojekt ausgelegt, damit sie vielfältige Erfahrungen sammeln und sich gegenseitig unterstützen können.

## Entwicklung einer Anwendungsidee

Einigen Sie sich im Team auf eine Idee für eine Anwendung. Gemeinsam durchlaufen Sie folgende Schritte:

1. Geben Sie Ihrer Idee einen griffigen Namen und beschreiben Sie die Idee in zwei, drei Sätzen
2. Entwerfen Sie Skizzen zur graphischen Oberfläche
3. Entwerfen Sie das Web-API (gemeint sind die HTTP-Requests und die -Responses inkl. ihrer Parameter, die die Anwendung verarbeiten kann; [Swagger](https://swagger.io/) mag Ihnen dabei helfen)
4. Entwickeln Sie ein Java- bzw. Kotlin-Interface (ein Programmier-Interface), das den hinter der Oberfläche liegenden Dienst bzw. die Anwendungslogik abbildet

Idee, UI-Skizzen, API und Interfaces bilden den geplanten "Kosmos" Ihrer Anwendung ab.

Sie sind vollkommen frei in der Projektidee. Achten Sie nur darauf, dass die Anwendungslogik selber nicht zu kompliziert ist und Sie vor allem algorithmisch nicht überfordert sind. Sie dürfen auch einen Web-Service umsetzen, der ein Web-API umsetzt und und ohne Oberfläche daherkommt.

* Umsetzung eines stapelbasierten Rechners
* Umsetzung eines Tic-Tac-Toe-Spiels mit dem Computer als "intelligenten" Gegner
* Entwicklung einer Lernkarten-Anwendung zum Selbstlernen
* Möglichkeiten, Webseiten um Diskussionsseiten zu bereichern
* Entwicklung eines Kurznachrichten-Diensts im Stil von Twitter
* Anwendung zum Erstellen von Umfragen samt Durchführung der Befragung
* Statischer Webseiten-Generator

> Ideen, die zu aufwendig scheinen, lassen sich als Prototyp bzw. Konzeptstudie oft sehr schlank umsetzen. Ideen, die zu einfach scheinen, lassen sich oft technologisch mit interessanten Features oder Umsetzungsvarianten anreichern; dazu mehr im folgenden Absatz.

## Umsetzung der Anwendungsidee

Wenn das geklärt ist, entwickeln Sie arbeitsteilig die Anwendung samt Dokumentation. Es liegen folgende Arbeitsbereiche vor, die sich teils sehr gut unabhängig voneinander entwickeln lassen.

* Clientseitige Oberfläche mit HTML/CSS
* Serverseitige Interaktion mit der Oberfläche (Interaktionsprototyp mit beispielhaften Interaktionsreaktionen)
* Anwendungsentwicklung via API ohne Oberfläche, getestet z.B. über `assert`s oder (bei Java) über die JShell (siehe Aufgabe 3)
* Kopplung von Oberfläche und Anwendungs-API, Testen der Anwendung
* Erstellung der Dokumentation samt Qualitätssicherung der Ausführungen

Sie sollten im Blick haben, dass es mehrere Aspekte gibt, die Javalin, Java und allgemeine Dienste/Frameworks bieten, die Ihnen das Leben sehr erleichtern können. Die folgende Liste dient als Anregung, beansprucht aber keine Vollständigkeit:

**Javalin**: Handler -- Cookies -- Context Extensions -- Validation -- Access Manager -- WebSockets -- Server-sent Events -- Request Logging -- Static Files -- Caching -- Asynchronous Requests -- JSON Mapper -- Views and Templates 

**Web-Programmierung allgemein**: Test und Monitoring von Web-Anwendungen -- automatisiertes Testen des Clients bzw. des Servers -- Nutzung von [Firebase](https://firebase.google.com/)-Diensten -- clientseite Nutzung einer Graphik-Bibliothek -- Clientdesign für Laptop und Smartphone mit responsivem CSS-Framework

**Java**: konsequenter Verzicht auf Schleifen (`for`/`while`) -- Arbeit mit Streams -- `Optional` statt `null` -- wenn sinnvoll: generische Programmierung

**Kotlin**: Schleifen (`for`/`while`) sind in Kotlin etwas anderer Natur als in Java; aber vielleicht versuchen Sie es, möglichst wenig von Ihnen Gebrauch zu machen -- Verwendung immutabler Datentypen -- möglichst keine `null`-Typen -- wenn sinnvoll: generische Programmierung

Die Wertigkeit Ihrer Projektleistung orientiert sich weniger an der Originalität der Anwendungsidee als vielmehr daran, einige dieser Aspekte bei der Umsetzung zu nutzen.

## Umfang & Abgabe

Es ist sehr schwer, generelle Angaben zum Projektumfang zu machen, da sich die Projekte sehr voneinander unterscheiden: die einen binden Dienste ein mit APIs, die einiges an Einarbeitung verlangen, aber dafür den Code erheblich verkürzen; die anderen setzen Algorithmen um, die halt einiges an Code erfordern. Dennoch ein Anhaltspunkt: Wenn Sie Ihren Code (HTML/CSS und Java bzw. Kotlin) ausdrucken und es kommen drei DIN A4-Seiten zusammen, dann ist das vermutlich zu wenig. Wenn es über 10 DIN A4-Seiten werden, dann ist das eventuell schon zuviel an Code.

Wir legen Wert auf eine ausgezeichnete Dokumentation, die Ihren Kommiliton:innen eine Hilfe ist, wenn sie sich für Ihr Projekt interessieren. Ihr Code sollte gut lesbar und nachvollziehbar sein.

Ihr Team liefert über eine für die Abgabe verantwortliche Person die Projektdateien ab. Wie gehabt ist das Projekt im `zip`-Format in Moodle hochzuladen. Die Verzeichnisstruktur orientiert sich an den für Gradle üblichen Konventionen. Abzuliefern ist ein auf das Nötigste bereinigte Verzeichnis aus Quellcode, Dokumentation und eventuellen Tests. Die Ausführung der Anwendung muss mit `gradle run` startbar sein. Der Einstiegspunkt für die Dokumentation ist `README.md`, von dort sind alle weiteren Dateien verlinkt. Orientieren Sie sich an Art und Stil der Readme-Datei an den Beispielen aus Aufgabe 1 bis 3.