# Quicksort mit Streams

Der Quicksort-Algorithmus beschreibt ein sehr effizientes Sortierverfahren. Rekursiv formuliert kommt der Algorithmus sehr elegant daher. Verbreitet ist die Beschreibung in der funktionalen Programmiersprache Haskell. Die Schreibweise in Haskell besticht in ihrer Kürze, die ohne ablenkende Details daherkommt.

~~~ haskell
quicksort [] = []
quicksort (x:xs) =
    let smallerSorted = quicksort [a | a <- xs, a <= x]
        biggerSorted = quicksort [a | a <- xs, a > x]
    in  smallerSorted ++ [x] ++ biggerSorted
~~~

Der `quicksort` angewendet auf die leere Liste `[]` liefert als Ergebnis eine leere Liste `[]`, siehe erste Zeile im Haskell-Code. Die zweite Zeile beschreibt die alternative Implementierung, wenn die Liste nicht leer ist. Die Schreibweise `(x:xs)` zerlegt eine übergebene Liste in ihr erstes Element `x` (das sogenannte Pivot-Element) und den Rest der Liste `xs`. Der Ausdruck `[a | a <- xs, a <= x]` stellt eine Liste zusammen aus den Elementen von `xs`, wobei nur die Elemente `a` in Frage kommen, die kleiner oder gleich dem Pivot-Element `x` sind. Die so durch den rekursiven Aufruf mit `quicksort` erstellte Liste ist die sortierte Liste `smallerSorted`; die Elemente dieser Liste sind notwendigerweise alle kleiner als das Pivot-Element. Analog wird die Liste `biggerSorted` erstellt. Das Sortierergebnis ist die Aneinanderkettung (Konkatenation, `++`) der Liste `smallerSorter` mit der Liste, die das Pivot-Element `x` enthält, und der Liste `biggerSorted`.

Es gibt einige Beispiele im Netz, in denen dieser Algorithmus in Java genau in dieser Weise aufgebaut ist. Wenn Streams verwendet werden, dann erfolgt ein Wechsel des Umgangs mit Listen und Strömen: Aus der Liste wird ein Stream gemacht, um per `filter` die Elemente zusammenzutragen, die kleiner/gleich bzw. größer als das Pivot-Element sind. Anschließend wird aus dem gefilterten Strom wieder eine Liste erzeugt. Beispiele dieses Vorgehens sind:

* "Functional Style QuickSort using Java 8 and Streams", https://gist.github.com/eloipereira/9ad98522ec5b49bcf04242e1ef9221a0
* "quick sort using Java 8 stream api", https://gist.github.com/chainone/b7f41152db5d711e110e1c73a4019ae8

Es gibt zwei Gründe, warum beide Beispiele zwischen Streams und Listen hin und her wechseln:

1. Nach einem Durchlauf durch die Elemente des Streams ist der Stream unbrauchbar für einen Wiederholungslauf; man kann mit einem Stream keinen `filter`-Durchlauf für die Elemente machen, die kleiner/gleich dem Pivot-Element sind, und _danach_ einen zweiten Durchlauf anschließen, der die größeren Elemente rausfiltert..
2. Man kann aus einem Stream nicht so ohne Weiteres das erste Element entfernen, um an das Pivot-Element zu kommen, und anschließend mit dem Rest der Liste weiterarbeiten.

Die folgende Umsetzung versteht sich als "Fingerübung", um trotz dieser Schwierigkeiten eine Quicksort-Umsetzung zu realisieren, die ausschließlich auf Streams basiert. Der Einfachheit halber beschränke ich die Implementierung auf `IntStreams`.

> Ich weiß, dass es eine `sorted`-Methode für Streams gibt. Hier steht die Übung im Vordergrund, den Quicksort mit Streams umzusetzen.

~~~ java
IntStream qsort(IntStream numbers) {
    IntStream.Builder smaller = IntStream.builder();
    IntStream.Builder greater = IntStream.builder();
    OptionalInt pivot = numbers.reduce((x,y) -> {
        if (y <= x) smaller.accept(y);
        else greater.accept(y);
        return x;
    });
    if (pivot.isEmpty()) return IntStream.empty();
    return IntStream.concat(qsort(smaller.build()),
                            IntStream.concat(IntStream.of(pivot.getAsInt()),
                                             qsort(greater.build())));
}
~~~

Der Kniff ist, im `reduce` als Seiteneffekt die beiden `IntStream.Builder` zu befüllen und das erste Elemente durchzuführen. Ist der `IntStream` aus `numbers` leer, dann wird `pivot` als `OptionalInt` ebenfalls leer sein und es ist ein leerer `IntStream` zurückzugeben; das ist der Abbruchfall der Rekursion. Ansonsten werden die Streams aus `smaller`, dem Pivot-Element und `greater` konkateniert zurückgeliefert.

Ein Beispiel-Aufruf zeigt, dass `qsort` läuft:

~~~ text
jshell> qsort(IntStream.of(3,1,6,4,0,7,3,9,2))
$11 ==> java.util.stream.IntPipeline$Head@5f2108b5

jshell> $11.forEachOrdered(System.out::println)
0
1
2
3
3
4
6
7
9
~~~

Die Fingerübung ist zwar gelungen, aber so ganz passen Streams an dieser Stelle nicht. Vor allem hebt diese Umsetzung nicht das Potenzial einer Parallelisierung (was bei Streams manchmal mit `parallel` so wunderbar einfach ist), was sich gerade beim Quicksort anbietet: Die Sortierungen für `smaller` und `greater` könnten problemlos parallel laufen.

Sie finden den Code unter [qsort.java](qsort.java).
